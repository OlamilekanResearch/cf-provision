# CF Provisioning Scripts

This repo contains a provisioning script that :

1. Creates two new EC2 compute resources in us-east-1
2. Creates an Elastic load Balancer (ELB) resource in front of the EC2 instances
3. Attaches the EC2 resources to the ELB resource
4. Provisions each resource with GP-3 SSD volumes, 10 gb of storage each


# Usage

1. Ensure cdk is installed. See https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html for installation guidelines.

2. Setup a virtualenv

3. Install the required dependencies with `pip install -r requirements.txt`

4. Bootstrap the script by running `cdk bootstrap --profile [AWS_PROFILE_NAME]`

5. Deploy the updated stack by running `cdk deploy --profile [AWS_PROFILE_NAME]`
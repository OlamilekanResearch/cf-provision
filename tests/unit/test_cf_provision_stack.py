import aws_cdk as core
import aws_cdk.assertions as assertions

from cf_provision.cf_provision_stack import CfProvisionStack

# example tests. To run these tests, uncomment this file along with the example
# resource in cf_provision/cf_provision_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = CfProvisionStack(app, "cf-provision")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })

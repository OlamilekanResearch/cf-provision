#!/usr/bin/env python3
import os

import aws_cdk as cdk

from cf_provision.cf_provision_stack import CfProvisionStack

app = cdk.App()
CfProvisionStack(app,
                 "CfProvisionStack",
                 env=cdk.Environment(account='683789135404', region='us-east-1'))

app.synth()

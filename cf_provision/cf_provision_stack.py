from aws_cdk import Stack, Duration
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_iam as iam, Size
from aws_cdk.aws_autoscaling import EbsDeviceVolumeType
from aws_cdk.aws_elasticloadbalancingv2 import ApplicationLoadBalancer, HealthCheck
from aws_cdk.aws_elasticloadbalancingv2_targets import InstanceIdTarget

from constructs import Construct


class CfProvisionStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        self.vpc = ec2.Vpc(self, "instance-vpc",
                           nat_gateways=0,
                           subnet_configuration=[
                               ec2.SubnetConfiguration(name="public", subnet_type=ec2.SubnetType.PUBLIC)]
                           )
        self.role = self.setup_role()

        base_instance = self.create_ec2_instance("base")
        second_instance = self.create_ec2_instance("second")

        lb = ApplicationLoadBalancer(self, "instance-load-balancer",
                                     vpc=self.vpc,
                                     internet_facing=True
                                     )

        listener = lb.add_listener("Listener",
                                   port=80,
                                   open=True
                                   )

        listener.add_targets("ApplicationFleet",
                             port=8080,
                             targets=[InstanceIdTarget(base_instance.instance_id),
                                      InstanceIdTarget(second_instance.instance_id)],
                             health_check=HealthCheck(path="/",
                                                      unhealthy_threshold_count=2,
                                                      healthy_threshold_count=5,
                                                      interval=Duration.seconds(30))
                             )

    def setup_role(self):
        role = iam.Role(self, "app-role", assumed_by=iam.ServicePrincipal("ec2.amazonaws.com"))
        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore"))
        return role

    @staticmethod
    def get_ami():
        return ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
        )

    def create_ec2_instance(self, instance_name):
        amzn_linux = self.get_ami()

        instance = ec2.Instance(self,
                                f"{instance_name}-ec2-instance",
                                machine_image=amzn_linux,
                                vpc=self.vpc,
                                role=self.role,
                                instance_type=ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE3, ec2.InstanceSize.SMALL)
                                )

        volume = ec2.Volume(self, f"{instance_name}-volume",
                            availability_zone="us-east-1a",
                            volume_type=EbsDeviceVolumeType.GP3,
                            size=Size.gibibytes(10))

        volume.grant_attach_volume_by_resource_tag(instance.grant_principal, [instance])

        return instance
